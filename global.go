package sglog

import (
	"errors"
	"sync"
)

// globVars holds all global variables. There is exactly one instance
// called globs. Package-private code should however not depend directly on globs,
// it should depend on the globVars structure only.
type globVars struct {
	defaultLogLevel Level
	loggers         map[string]*Logger
	loggersMut      sync.Mutex
	backend         Backend
	backendMut      sync.Mutex
}

var defaultBackend Backend = new(StderrBackend)

var internalLog = GetLogger("gitlab.com/sglog/sglog")

var globs = globVars{
	defaultLogLevel: Debug,
	loggers:         make(map[string]*Logger),
	backend:         defaultBackend,
}

// GetLogger gets a Logger instance registered under pkgPath key. Even though the pkgPath key can be
// any string, it is strongly encouraged to use package path as the key so other applications
// can access the Logger and set its level etc.
// If there is not yet a Logger instance registered under pkgPath key, new one shall be created.
func GetLogger(pkgPath string) *Logger {
	return globs.loggerFromGlobalMap(pkgPath)
}

// UseBackend sets given backend to be used by all loggers.
// If the backend cannot be used, the default one shall be used instead and a warning message shall be logged into it.
func UseBackend(backend Backend) {
	err := globs.setGlobalBackend(backend)
	if err != nil {
		internalLog.Warning("Unable to use given backend, error: %v.", err)
	}
}

func SetDefaultLogLevel(defaultlvl Level) {
	globs.defaultLogLevel = defaultlvl
}

func GetDefaultLogLevel() Level {
	return globs.defaultLogLevel
}

func (vars *globVars) loggerFromGlobalMap(pkgPath string) (result *Logger) {
	vars.loggersMut.Lock()
	defer vars.loggersMut.Unlock()
	var contains bool
	result, contains = vars.loggers[pkgPath]
	if !contains {
		result = newLogger(pkgPath, vars)
		vars.loggers[pkgPath] = result
	}
	return
}

func (vars *globVars) getGlobalBackend() Backend {
	vars.backendMut.Lock()
	defer vars.backendMut.Unlock()
	return vars.backend
}

func (vars *globVars) setGlobalBackend(newBackend Backend) (err error) {
	if newBackend == nil {
		err = errors.New("cannot use nil logging backend")
	} else {
		vars.backendMut.Lock()
		defer vars.backendMut.Unlock()
		if vars.backend != nil {
			vars.backend.Close()
		}
		vars.backend = newBackend
	}
	return
}
