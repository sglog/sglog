package sglog

import (
	"encoding/binary"
	"fmt"
	"log/syslog"
	"net"
	"path"
)

type JournaldBackend struct {
	journal net.Conn
}

const defaultJournalSock = "/run/systemd/journal/socket"

// DefaultJournaldBackend creates a new JournaldBackend that uses the default journald socket.
// The actual socket address is /run/systemd/journal/socket.
// Non-nil error is returned only if a connection to the default journald socket can't be created.
func DefaultJournaldBackend() (result *JournaldBackend, err error) {
	return NewJournaldBackend(defaultJournalSock)
}

// NewJournaldBackend creates a new JournaldBackend that uses provided socket address to connect to journald.
// Non-nil error is returned only if a connection to the provided socket address can't be created.
func NewJournaldBackend(sock string) (result *JournaldBackend, err error) {
	result = new(JournaldBackend)
	result.journal, err = net.Dial("unixgram", sock)
	return
}

func (jba *JournaldBackend) Log(entry *LogEntry) (err error) {
	msg := fmt.Sprintf(
		"PRIORITY=%d\n"+
			"CODE_FILE=%s\n"+
			"CODE_LINE=%d\n"+
			"MESSAGE\n"+
			"%s%s\n",
		lvlToPriority(entry.Level),
		path.Join(entry.PkgPath, entry.File),
		entry.Line,
		encodeLen(len(entry.Message)), entry.Message)
	_, err = jba.journal.Write([]byte(msg))
	return
}

func (jba *JournaldBackend) Close() {
	jba.journal.Close() // Ignoring possible error.
}

// lvlToPriority translates sglog level to value that can be used in the journald Priority field.
// The journald Priority field is compatible with syslog, that is why we use constants from the syslog package.
func lvlToPriority(level Level) syslog.Priority {
	switch level {
	default:
		fallthrough
	case Debug:
		return syslog.LOG_DEBUG
	case Info:
		return syslog.LOG_INFO
	case Warning:
		return syslog.LOG_WARNING
	}
}

func encodeLen(len int) []byte {
	result := make([]byte, 8)
	binary.LittleEndian.PutUint64(result, uint64(len))
	return result
}
