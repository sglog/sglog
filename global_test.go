package sglog

import (
	"reflect"
	"testing"
)

func TestLoggerFromGlobalMap(t *testing.T) {
	testVars := globVars{
		defaultLogLevel: Info,
		loggers:         make(map[string]*Logger),
	}
	serverOrg1 := testVars.loggerFromGlobalMap("server.org/shall/be/created")
	if serverOrg1 == nil {
		t.Fatal("loggerFromGlobalMap returned nil.")
	}
	serverOrg2 := testVars.loggerFromGlobalMap("server.org/shall/be/created")
	if serverOrg1 != serverOrg2 {
		t.Fatal("loggerFromGlobalMap returned two different instances for the same key.")
	}
	empty := testVars.loggerFromGlobalMap("")
	if empty == nil {
		t.Fatal("loggerFromGlobalMap returned nil.")
	}
	if testVars.defaultLogLevel != serverOrg1.GetLevel() ||
		testVars.defaultLogLevel != serverOrg2.GetLevel() ||
		testVars.defaultLogLevel != empty.GetLevel() {
		t.Fatal("loggerFromGlobalMap created a Logger with wrong logging level.")
	}
}

func TestSetGlobalBackend(t *testing.T) {
	// backend is nil
	testVars := globVars{}
	stderr1 := new(StderrBackend)
	stderr2 := new(StderrBackend)
	setNil1 := testVars.setGlobalBackend(nil)
	if testVars.getGlobalBackend() != nil {
		t.Fatal("Expected nil Backend but got:", testVars.getGlobalBackend())
	}
	if setNil1 == nil {
		t.Fatal("setGlobalBackend accepted nil Backend.")
	}
	setAndCheckBackend(t, &testVars, stderr1)

	setNil2 := testVars.setGlobalBackend(nil)
	if setNil2 == nil {
		t.Fatal("setGlobalBackend accepted nil Backend.")
	}
	if testVars.getGlobalBackend() != stderr1 {
		t.Fatal("setGlobalBackend did not accept nil backend, but the Backend was changed.")
	}
	setAndCheckBackend(t, &testVars, stderr2)
}

func TestBackendClosing(t *testing.T) {
	testVars := globVars{}
	stderr := new(StderrBackend)
	close1 := new(mockCloseBackend)
	close2 := new(mockCloseBackend)
	setAndCheckBackend(t, &testVars, close1)
	setAndCheckBackend(t, &testVars, close2)
	setAndCheckBackend(t, &testVars, stderr)
	if !close1.closed || !close2.closed {
		t.Fatal("Backends were not properly closed.")
	}
}

func TestBackendRuntimeChange(t *testing.T) {
	testVars := globVars{
		defaultLogLevel: Warning,
		loggers:         make(map[string]*Logger),
	}
	close1 := new(mockCloseBackend)
	close2 := new(mockCloseBackend)
	logger1 := testVars.loggerFromGlobalMap("server.org/one")
	setAndCheckBackend(t, &testVars, close1)
	logger2 := testVars.loggerFromGlobalMap("server.org/two")
	logger1.Info("Info won't be logged.")
	logger1.Warning("Warning will be logged.")
	logger2.Warning("Warning will be logged.")
	setAndCheckBackend(t, &testVars, close2)
	logger1.Info("Info won't be logged.")
	logger1.Warning("Warning will be logged.")
	logger2.Warning("Warning will be logged.")
	logger2.Debug("Debug won't be logged.")
	logger1.Warning("Warning will be logged.")
	if !close1.closed {
		t.Fatal("Backend wasn't properly closed.")
	}
	if close1.usedAfterClose {
		t.Fatal("Backend was used after close.")
	}
	if close1.recvEntries != 2 {
		t.Fatal("Backend did not receive expected number of log entries.")
	}
	if close2.closed {
		t.Fatal("Backend was unexpectedly closed.")
	}
	if close2.recvEntries != 3 {
		t.Fatal("Backend did not receive expected number of log entries.")
	}
}

func setAndCheckBackend(t *testing.T, testVars *globVars, backend Backend) {
	setErr := testVars.setGlobalBackend(backend)
	if setErr != nil {
		t.Fatalf("setGlobalBackend did not accept %s due to: %v", reflect.TypeOf(backend), setErr)
	}
	if testVars.getGlobalBackend() != backend {
		t.Fatalf("setGlobalBackend to %s returned success, but the Backend was not set", reflect.TypeOf(backend))
	}
}

type mockCloseBackend struct {
	closed         bool
	usedAfterClose bool
	recvEntries    int
}

func (mcb *mockCloseBackend) Log(entry *LogEntry) error {
	mcb.recvEntries++
	if mcb.closed {
		mcb.usedAfterClose = true
	}
	return nil
}

func (mcb *mockCloseBackend) Close() {
	mcb.closed = true
}
