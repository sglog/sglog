package sglog

import (
	"bytes"
	"encoding/binary"
	"log/syslog"
	"net"
	"os"
	"path"
	"strconv"
	"strings"
	"testing"
	"time"
)

const (
	testSocketName = "testsock"
	testPkgName    = "testpkg"
)

func TestJournalClose(t *testing.T) {
	name1 := testSocketName + "1"
	name2 := testSocketName + "2"
	conn1 := createSocket(t, name1)
	defer conn1.Close()
	defer os.Remove(name1)
	conn2 := createSocket(t, name2)
	defer conn2.Close()
	defer os.Remove(name2)
	backend1, bErr1 := NewJournaldBackend(name1)
	backend2, bErr2 := NewJournaldBackend(name2)
	backend3, bErr3 := DefaultJournaldBackend()
	if bErr1 != nil || bErr2 != nil || bErr3 != nil {
		t.Fatal("Unable to create all backends", bErr1, bErr2, bErr3)
	}
	conn1.Close()
	os.Remove(name1)
	backend1.Close()
	backend2.Close()
	backend3.Close()
}

func TestNonExistentSocket(t *testing.T) {
	_, err := NewJournaldBackend("thissockdoesnotexist")
	if err == nil {
		t.Fatal("Connecting to non existent journald socket was successful.")
	}
}

func TestLoggingToClosed(t *testing.T) {
	conn := createSocket(t, testSocketName)
	defer conn.Close()
	defer os.Remove(testSocketName)
	journald, err := NewJournaldBackend(testSocketName)
	if err != nil {
		t.Fatal("Unable to connect to unix dgram socket", testSocketName)
	}
	conn.Close()
	logErr := journald.Log(&LogEntry{
		Level:   Warning,
		PkgPath: testPkgName,
		File:    "journald_test.go",
		Line:    10,
		Message: "This message should not be logged.",
	})
	if logErr == nil {
		t.Fatal("Logging to removed socket was successful.")
	}
}

var testSockJournalInput = []struct {
	level            Level
	msg              string
	expectedPriority int
}{
	{Warning, "Short message.", int(syslog.LOG_WARNING)},
	{Info, "", int(syslog.LOG_INFO)},
	{Debug, "Kind of a long message.\n" +
		"It is not the longest message ever, but it is quite long, and it contains line breaks. Such as this one:\n" +
		"Despite that\n\n, it should work.", int(syslog.LOG_DEBUG)},
	{Warning, "Message ending with a line break.\n", int(syslog.LOG_WARNING)},
}

func TestSockJournal(t *testing.T) {
	conn := createSocket(t, testSocketName)
	defer conn.Close()
	defer os.Remove(testSocketName)
	journald, err := NewJournaldBackend(testSocketName)
	if err != nil {
		t.Fatal("Unable to connect to unix dgram socket", testSocketName)
	}
	UseBackend(journald)
	go logMessages()
	for _, in := range testSockJournalInput {
		verifyDgram(t, read1024(t, conn), in.msg, in.expectedPriority)
	}
}

func createSocket(t *testing.T, sockName string) net.PacketConn {
	conn, err := net.ListenPacket("unixgram", sockName)
	if err != nil {
		t.Fatal("Unable to create unix dgram socket", sockName)
	}
	conn.SetDeadline(time.Time{})
	return conn
}

func read1024(t *testing.T, conn net.PacketConn) []byte {
	buf := make([]byte, 1024)
	len, _, rdErr := conn.ReadFrom(buf)
	if rdErr != nil {
		t.Fatal("Unable to read from socket:", rdErr)
	}
	return buf[:len]
}

func logMessages() {
	logger := GetLogger(testPkgName)
	for _, in := range testSockJournalInput {
		switch in.level {
		default:
			fallthrough
		case Debug:
			logger.Debug(in.msg)
		case Info:
			logger.Info(in.msg)
		case Warning:
			logger.Warning(in.msg)
		}
	}
}

func verifyDgram(t *testing.T, dgram []byte, msg string, priority int) {
	fields := make(map[string]string)
	buf := bytes.NewBuffer(dgram)
	var rdErr error
	for {
		var line []byte
		line, rdErr = buf.ReadBytes('\n')
		if rdErr != nil {
			break
		}
		keyval := strings.Split(string(line[:len(line)-1]), "=")
		switch len(keyval) {
		case 2:
			fields[keyval[0]] = keyval[1]
		case 1:
			fields[keyval[0]] = readJournalBlob(t, buf)
		default:
			t.Fatal("Unexpected format of a line:", line)
		}
	}
	checkField(t, "MESSAGE", msg, fields)
	checkField(t, "PRIORITY", strconv.Itoa(priority), fields)
	checkField(t, "CODE_FILE", path.Join(testPkgName, "journald_test.go"), fields)
	checkCodeLineField(t, fields)
	if len(fields) != 0 {
		t.Fatal("Unexpected journald fields:", fields)
	}
}

func readJournalBlob(t *testing.T, buf *bytes.Buffer) string {
	len := make([]byte, 8)
	readLen, rdLenErr := buf.Read(len)
	if rdLenErr != nil {
		t.Fatal("Unable to read journal blob length:", rdLenErr)
	}
	if readLen != 8 {
		t.Fatal("Journal blob length is not 8 but", readLen)
	}
	lenUint := binary.LittleEndian.Uint64(len)
	blob := make([]byte, lenUint)
	blobLen, rdBlobErr := buf.Read(blob)
	if rdBlobErr != nil {
		t.Fatal("Unable to read journal blob:", rdBlobErr)
	}
	if blobLen != int(lenUint) {
		t.Fatalf("Expected journal blob length %d, but got %d.", lenUint, blobLen)
	}
	delim, delimErr := buf.ReadByte()
	if delimErr != nil {
		t.Fatal("Unable to read the new line delimiter.")
	}
	if delim != '\n' {
		t.Fatal("Expected a new line delimiter but got:", delim)
	}
	return string(blob)
}

func checkField(t *testing.T, expectedName string, expectedValue string, fields map[string]string) {
	if fields[expectedName] == expectedValue {
		delete(fields, expectedName)
	} else {
		t.Fatalf("Field %s is not %s in map %v", expectedName, expectedValue, fields)
	}
}

func checkCodeLineField(t *testing.T, fields map[string]string) {
	codeFile, ok := fields["CODE_LINE"]
	if !ok {
		t.Fatal("CODE_LINE field not present.")
	}
	_, err := strconv.Atoi(codeFile)
	if err != nil {
		t.Fatal("CODE_LINE field is not a number: ", codeFile)
	}
	delete(fields, "CODE_LINE")
}
