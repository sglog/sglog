package sglog

import (
	"fmt"
	"testing"
)

func TestYAMLExample1(t *testing.T) {
	testVars := globVarsForTest()
	loadErr := loadConfig("confexamples/confexample1.yaml", &testVars)
	if loadErr != nil {
		t.Fatal(loadErr)
	}
	if testVars.defaultLogLevel != Debug {
		t.Error("Wrong default log level: ", testVars.defaultLogLevel)
	}
	expected := []expectedPair{
		{"bitbucket.org/project/package", Warning},
		{"gitlab.com/project/package", Info},
		{"justaname", Debug},
	}
	if err := checkLoggers(&testVars, expected); err != nil {
		t.Error(err)
	}
}

func TestYAMLExample2(t *testing.T) {
	testVars := globVarsForTest()
	loadErr := loadConfig("confexamples/confexample2.yaml", &testVars)
	if loadErr != nil {
		t.Fatal(loadErr)
	}
	if testVars.defaultLogLevel != Info {
		t.Error("Wrong default log level: ", testVars.defaultLogLevel)
	}
	expected := []expectedPair{
		{"bitbucket.org/mysw/currentlydebugging", Debug},
		{"bitbucket.org/mysw/noisyinfo", Warning},
		{"bitbucket.org/somelibrary/whynotrepeatthedefault", Info},
		{"main", Debug},
	}
	if err := checkLoggers(&testVars, expected); err != nil {
		t.Error(err)
	}
}

var testYAMLBadVersionData = []string{
	"confexamples/badversion.yaml",
	"confexamples/noversion.yaml",
}

func TestYAMLBadVersions(t *testing.T) {
	for _, confFile := range testYAMLBadVersionData {
		testVars := globVarsForTest()
		loadErr := loadConfig(confFile, &testVars)
		if loadErr == nil {
			t.Error("Expected error when loading", confFile)
		}
	}
}

type expectedPair struct {
	name string
	lvl  Level
}

func checkLoggers(testVars *globVars, expected []expectedPair) error {
	if len(testVars.loggers) != len(expected) {
		return fmt.Errorf("expected %d loggers got %d instead", len(expected), len(testVars.loggers))
	}
	for _, pair := range expected {
		actualLvl := testVars.loggers[pair.name].level
		if actualLvl != pair.lvl {
			return fmt.Errorf("expected level %d for package %s, but got level %d", pair.lvl, pair.name, actualLvl)
		}
	}
	return nil
}

func globVarsForTest() globVars {
	return globVars{
		defaultLogLevel: Debug,
		loggers:         make(map[string]*Logger),
	}
}
