package sglog

import (
	"fmt"
	"path"
	"runtime"
)

// Logger represents an object that can be used for logging by single package.
// The instances of Logger should be acquired by calling GetLogger function.
type Logger struct {
	// Key under which the Logger is registered.
	// Specified by the user, when calling GetLogger. Should be full package path.
	pkgPath string
	// Shortened pkgPath. Used in log lines.
	pathStrip string
	// Any log message with Level lower than level is not logged.
	level Level
	// Reference to globVars holding backend and such, the global globs are not used directly.
	globs *globVars
}

// SetLevel sets a logging level of the Logger to given level.
// Messages with lower priority than the logging level of the logger shall
// not be logged.
func (logger *Logger) SetLevel(level Level) {
	if level.isValid() {
		logger.level = level
	}
}

// GetLevel returns the current logging level of the Logger.
// Messages with lower priority than the logging level of the logger shall
// not be logged.
func (logger *Logger) GetLevel() Level {
	return logger.level
}

// Debug logs a message under a Debug log level.
// Arguments are handled in the manner of fmt.Printf.
func (logger *Logger) Debug(format string, a ...interface{}) {
	logger.log(Debug, format, a...)
}

// Info logs a message under an Info log level.
// Arguments are handled in the manner of fmt.Printf.
func (logger *Logger) Info(format string, a ...interface{}) {
	logger.log(Info, format, a...)
}

// Warning logs a message under a Warning log level.
// Arguments are handled in the manner of fmt.Printf.
func (logger *Logger) Warning(format string, a ...interface{}) {
	logger.log(Warning, format, a...)
}

func newLogger(pkgPath string, globs *globVars) (result *Logger) {
	result = new(Logger)
	result.pkgPath = pkgPath
	result.pathStrip = stripPkgPath(pkgPath)
	// We care about defaultLogLevel at this time, so we store it.
	result.level = globs.defaultLogLevel
	// We care about current Backend every time a log entry is being logged. We store globs, so we can
	// call globs.getGlobalBackend.
	result.globs = globs
	return
}

func stripPkgPath(pkgPath string) string {
	rest, last := path.Split(pkgPath)
	if rest == "" || rest == "/" {
		return last
	}
	nextToLast := path.Base(rest)
	return fmt.Sprintf("%s/%s", nextToLast, last)
}

// callerSkipFromLog holds number of frames that we need to ascend from the log method to the
// actual code that wanted to log a message.
const callerSkipFromLog = 2

func (logger *Logger) log(level Level, format string, a ...interface{}) {
	if level < logger.level {
		return
	}
	// If we do not recover the information then they won't be part of the LogEntry - no need to handle it
	// in any other way.
	_, filePath, line, _ := runtime.Caller(callerSkipFromLog)
	_, fileName := path.Split(filePath)
	entry := LogEntry{
		Level:   level,
		PkgPath: logger.pathStrip,
		File:    fileName,
		Line:    line,
		Message: fmt.Sprintf(format, a...),
	}
	logger.passEntryToBackend(&entry)
}

func (logger *Logger) passEntryToBackend(entry *LogEntry) {
	backendInUse := logger.globs.getGlobalBackend()
	err := backendInUse.Log(entry)
	if err != nil && backendInUse != defaultBackend {
		// Ignoring possible error.
		// Default should be as safe to use as possible.
		defaultBackend.Log(entry)
	}
}
